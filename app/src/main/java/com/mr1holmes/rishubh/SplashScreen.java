package com.mr1holmes.rishubh;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class SplashScreen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        Typeface tf = Typeface.createFromAsset(getAssets(), "Roboto-Regular.ttf");
        TextView textView = (TextView) findViewById(R.id.title_tv);
        textView.setTypeface(tf);

        TextInputEditText editText = (TextInputEditText) findViewById(R.id.number_edittext);
        editText.setTypeface(tf);

        Button loginButton = (Button) findViewById(R.id.login_btn);
        loginButton.setTypeface(tf);

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SplashScreen.this, PhotoCaptureActivity.class);
                startActivity(intent);
            }
        });

    }
}
